import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<void> signInWithGoogle() async {
    final googleSignIn = GoogleSignIn();
    final googleUser = await googleSignIn.signIn();

    if(googleUser != null) {
      final googleAuth = await googleUser.authentication;

      if(googleAuth.idToken != null) {
        final userCredential = await _firebaseAuth.signInWithCredential(
          GoogleAuthProvider.credential(
            idToken: googleAuth.idToken,
            accessToken: googleAuth.accessToken
          )
        );
        return userCredential.user;
      }
    }
    else {
      throw FirebaseAuthException(
        message: "Sign in aborted by the User",
        code: "ERROR_ABORTED_BY_USER"
      );
    }
  }

  Future<void> googleSignOut() async {
    final googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();
    await _firebaseAuth.signOut();
  }
}