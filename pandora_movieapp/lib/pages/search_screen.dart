import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'detail_screen.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:avatar_glow/avatar_glow.dart';

class Search extends StatefulWidget {
  static TextEditingController searchController = TextEditingController(text: '');

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> with AutomaticKeepAliveClientMixin<Search>{
  List searchResult = [];
  var filledQuery;

  getSearch() async {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/search/movie?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&query=' + Search.searchController.text + '&page=1&include_adult=false'
    ));
    var jsonData = jsonDecode(response.body);

    searchResult = jsonData['results'];

    print(searchResult);
    print(Search.searchController.text);
    return searchResult;
  }

  /*@override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getSearch();     
    });
  }*/

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        child: Container(
          padding: const EdgeInsets.fromLTRB(16.0, 80.0, 16.0, 16.0),
          child: Column(
            children: [
              SearchForm(
                onSearch: (q) {
                  setState(() {
                    Search.searchController.text = q;                 
                  });
                }
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: FutureBuilder(
                    future: getSearch(),
                    builder: (context, snapshot) {
                      if (snapshot.data == null) {
                        return Container();
                      }
                      else if(snapshot.hasData) {
                        return ListView.builder(
                          itemCount: searchResult.length,
                          itemBuilder: (context, index) {
                            return searchResult[index]['poster_path'] == null ? Container() : searchResult[index]['backdrop_path'] == null ? Container() : searchResult[index]['overview'] == null ? Container() : InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return DetailScreen(
                                        title: searchResult[index]['title'],
                                        bannerURL: 'https://image.tmdb.org/t/p/w500' + searchResult[index]['backdrop_path'],
                                        posterURL: 'https://image.tmdb.org/t/p/w500' + searchResult[index]['poster_path'],
                                        synopsis: searchResult[index]['overview'],
                                        rating: searchResult[index]['vote_average'].toString(),
                                        releaseDate: searchResult[index]['release_date'],
                                        id: searchResult[index]['id'].toString(),
                                        language: searchResult[index]['original_language']
                                      );
                                    }
                                  )
                                );
                              },
                              child: Container(
                                height: 200,
                                padding: const EdgeInsets.only(bottom: 8, top: 8),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 140,
                                      child: Container(
                                        height: 200,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: NetworkImage(
                                              'https://image.tmdb.org/t/p/w500' + searchResult[index]['poster_path']
                                            )
                                          )
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Flexible(
                                            child: Text(
                                              searchResult[index]['title'],
                                              style: TextStyle(
                                                color: Colors.lightBlue,
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                              textAlign: TextAlign.end,
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    "Release Date: ",
                                                    style: TextStyle(
                                                      color: Colors.lightBlue,
                                                      fontSize: 12
                                                    ),
                                                  ),
                                                  Text(
                                                    searchResult[index]['release_date'],
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 12
                                                    ),
                                                  ),
                                                  SizedBox(width: 3),
                                                  Container(
                                                    color: Colors.lightBlue,
                                                    width: 1,
                                                    height: 10,
                                                  ),
                                                  SizedBox(width: 3),
                                                  Text(
                                                    "Rating: ",
                                                    style: TextStyle(
                                                      color: Colors.lightBlue,
                                                      fontSize: 12
                                                    ),
                                                  ),
                                                  searchResult[index]['vote_average'] == 0 ? Text(
                                                      "-",
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 12
                                                      ),
                                                    ) : Text(
                                                    searchResult[index]['vote_average'].toString(),
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 12
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 15),
                                          Flexible(
                                            child: Text(
                                              searchResult[index]['overview'],
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.justify,
                                              maxLines: 4,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                              ),
                                            ),
                                          )
                                        ]
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            );
                          }
                        );
                      }
                      return Text(
                        'Error: ${snapshot.error}'
                      );
                    },
                  ),
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class SearchForm extends StatefulWidget {
  final void Function(String search) onSearch;

  const SearchForm({ Key key, this.onSearch }) : super(key: key);

  @override
  _SearchFormState createState() => _SearchFormState();
}

class _SearchFormState extends State<SearchForm> {
  final _formKey = GlobalKey<FormState>();

  var _autoValidate = false;
  var _search;
  bool _isListening = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key:  _formKey,
        //autovalidate: _autoValidate,
        child: TextFormField(
                style: TextStyle(
                  color: Colors.lightBlue
                ),
                controller: Search.searchController,
                decoration: InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(
                    color: Colors.lightBlue
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Colors.lightBlue,
                      width: 2.0
                    )
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Colors.lightBlue,
                      width: 2.0
                    )
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.lightBlue,
                    ), 
                    onPressed: () {
                      final isValid = _formKey.currentState.validate();
                      if(isValid) {
                        widget.onSearch(_search);
                        FocusManager.instance.primaryFocus.unfocus();
                      }
                      else {
                        setState(() {
                          _autoValidate = true;                          
                        });
                      }
                    },
                  ),
                  /*Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                        icon: AvatarGlow(
                          animate: false,
                          glowColor: Colors.white,
                          endRadius: 80,
                          duration: Duration(milliseconds: 2800),
                          repeatPauseDuration: Duration(milliseconds: 100),
                          repeat: true,
                          child: Icon(
                            Icons.mic,
                            color: Colors.lightBlue
                          )
                        ), 
                        onPressed: () {
                          
                        }
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.search,
                          color: Colors.lightBlue,
                        ), 
                        onPressed: () {
                          final isValid = _formKey.currentState.validate();
                          if(isValid) {
                            widget.onSearch(_search);
                            FocusManager.instance.primaryFocus.unfocus();
                          }
                          else {
                            setState(() {
                              _autoValidate = true;                          
                            });
                          }
                        },
                      ),
                    ],
                  ),*/
                ),
                onChanged: (value) {
                  _search = value;
                },
                validator: (value) {
                  if(value.isEmpty) {
                    return 'Please provide a proper search query';
                  }
                  return null;
                },
              ),
      ),
    );
  }
}

class Speech extends StatefulWidget {
  @override
  _SpeechState createState() => _SpeechState();
}

class _SpeechState extends State<Speech> {
  stt.SpeechToText _speech;
  bool isListening = false;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      
    );
  }
}