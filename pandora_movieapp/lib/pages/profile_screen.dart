import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pandora_movieapp/authentication/google_sign_in.dart';
import 'package:pandora_movieapp/pages/login_screen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with AutomaticKeepAliveClientMixin<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: ProfileScreenComponents()
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ProfileScreenComponents extends StatefulWidget {
  @override
  _ProfileScreenComponentsState createState() => _ProfileScreenComponentsState();
}

class _ProfileScreenComponentsState extends State<ProfileScreenComponents> {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  
  Future<void> _signOut() async {
    await _firebaseAuth.signOut();
  }

  Future<void> googleUserSignOut() async {
    await GoogleAuth().googleSignOut();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(16, 160, 16, 16),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                padding: const EdgeInsets.only(top: 40, bottom: 40),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.lightBlue,
                ),
                child: Center(
                  child: Text(
                    /*_firebaseAuth.currentUser.displayName == null ?*/ _firebaseAuth.currentUser.email[0].toUpperCase(), /*: _firebaseAuth.currentUser.displayName[0].toUpperCase(),*/
                    style: TextStyle(
                      fontSize: 57,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Center(
                child: Text(
                  /*_firebaseAuth.currentUser.displayName == null ?*/ _firebaseAuth.currentUser.email, /*: _firebaseAuth.currentUser.displayName,*/
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.lightBlue,
                    fontWeight: FontWeight.bold
                  ),
                ),
              )
            ),
            SizedBox(height: 10),
            Card(
              color: Colors.lightBlue,
                child: Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0, left: 8.0, right: 8.0),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: Colors.black,
                                ),
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "PREMIUM",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.lightBlue
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {

                                },
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.history
                                    ),
                                    SizedBox(height: 5.0),
                                    Text(
                                      'Recently Watched',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      ),
                                    )
                                  ],
                                ), 
                              ),
                              InkWell(
                                onTap: () {
                                    
                                },
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.favorite_outline
                                    ),
                                    SizedBox(height: 5.0),
                                    Text(
                                      'Favorites',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      ),
                                    )
                                  ]
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
            ),
            SizedBox(height: 20),
            InkWell(
              onTap: () {

              },
              child: Row(
                children: [
                  Icon(
                    Icons.edit,
                    color: Colors.lightBlue
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Edit Profile",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.lightBlue
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              onTap: () {

              },
              child: Row(
                children: [
                  Icon(
                    Icons.settings,
                    color: Colors.lightBlue
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Settings",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.lightBlue
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              onTap: () {
                googleUserSignOut().then(
                  (value) => Navigator.pushAndRemoveUntil(
                    context, 
                    MaterialPageRoute(
                      builder: (context) => LoginScreen()
                    ), 
                    (route) => false
                  )
                );
              },
              child: Row(
                children: [
                  Icon(
                    Icons.logout,
                    color: Colors.lightBlue
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Log Out",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.lightBlue
                    ),
                  )
                ],
              ),
            ),
          ]
        )
      );
  }
}
