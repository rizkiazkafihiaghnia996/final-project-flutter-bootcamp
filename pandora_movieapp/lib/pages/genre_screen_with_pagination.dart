import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_paginator/flutter_paginator.dart';
import 'package:http/http.dart' as http;
import 'detail_screen.dart';

class GenreMovies {
  List<dynamic> movies;
  int statusCode;
  String errorMessage;
  int totalResults;
  int nItems;

  GenreMovies.fromResponse(http.Response response) {
    this.statusCode = response.statusCode;
    Map jsonData = json.decode(response.body);
    movies = jsonData['results'];
    totalResults = jsonData['total_results'];
    nItems = movies.length;
  }

  GenreMovies.withError(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}

class GenreScreenV2 extends StatefulWidget {
  final String genreID, genreName;
  const GenreScreenV2({ Key key, this.genreID, this.genreName }) : super(key: key);

  @override
  _GenreScreenV2State createState() => _GenreScreenV2State();
}

class _GenreScreenV2State extends State<GenreScreenV2> {
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();

  Future<GenreMovies> getGenreMovies(int page) async {
    print('Page ${page}');
    try {
      var response = await http.get(Uri.parse(
        'https://api.themoviedb.org/3/discover/movie?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page.toString()}&with_genres=${widget.genreID}&with_watch_monetization_types=flatrate'
      ));
      print(response.body);
      return GenreMovies.fromResponse(response);
    }
    catch (e) {
      if (e is IOException) {
        return GenreMovies.withError(
          'Please Check Your Internet Connection'
        );
      }
      else {
        print(e.toString());
        return GenreMovies.withError(
          'Something Went Wrong'
        );
      }
    }
  }

  List<dynamic> pageItemsGetter(GenreMovies genreMovies) {
    List genreMoviesList = [];
    genreMoviesList = genreMovies.movies;
    return genreMoviesList;
  }

  Widget listItemBuilder(value, index) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return DetailScreen(
                title: value['title'],
                bannerURL: 'https://image.tmdb.org/t/p/w500' + value['backdrop_path'],
                posterURL: 'https://image.tmdb.org/t/p/w500' + value['poster_path'],
                synopsis: value['overview'],
                rating: value['vote_average'].toString(),
                releaseDate: value['release_date'],
                id: value['id'].toString(),
                language: value['original_language']
              );
            }
          )
        );             
      },
      child: Container(
        width: 140,
        child: Container(
          height: 200,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  'https://image.tmdb.org/t/p/w500' + value['poster_path'] == null ? Container() : 'https://image.tmdb.org/t/p/w500' + value['poster_path']
                )
              )
            ),
          ),
        ),
      );
  }

  Widget loadingWidgetBuilder() {
    return Container(
      alignment: Alignment.center,
      height: 160.0,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
      ),
    );
  }

  Widget errorWidgetBuilder(GenreMovies genreMovies, retryListener) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Text(
            genreMovies.errorMessage,
            style: TextStyle(
              color: Colors.lightBlue,
              fontSize: 16
            ),
          ),
        ),
        InkWell(
          onTap: retryListener,
          child: Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.lightBlue,
              borderRadius: BorderRadius.circular(10)
            ),
            child: Text(
              "Retry",
              style: TextStyle(
                color: Colors.black,
                fontSize: 14
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget emptyListWidgetBuilder(GenreMovies genreMovies) {
    return Center(
      child: Text(
        "Error 404: No Movies Found",
        style: TextStyle(
          color: Colors.lightBlue,
          fontSize: 14
        ),
      ),
    );
  }

  int totalItemsGetter(GenreMovies genreMovies) {
    return genreMovies.totalResults;
  }

  bool pageErrorChecker(GenreMovies genreMovies) {
    return genreMovies.statusCode != 200;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          widget.genreName,
          style: TextStyle(
            fontWeight: FontWeight.bold
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(2),
          child: Container(
            height: 2,
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Paginator.gridView(
          key: paginatorGlobalKey,
          pageLoadFuture: getGenreMovies, 
          pageItemsGetter: pageItemsGetter, 
          listItemBuilder: listItemBuilder, 
          loadingWidgetBuilder: loadingWidgetBuilder, 
          errorWidgetBuilder: errorWidgetBuilder, 
          emptyListWidgetBuilder: emptyListWidgetBuilder, 
          totalItemsGetter: totalItemsGetter, 
          pageErrorChecker: pageErrorChecker,
          scrollPhysics: BouncingScrollPhysics(), 
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 150,
            childAspectRatio: 2/3,
            crossAxisSpacing: 10,
            mainAxisSpacing: 15
          ),
        ),
      ),
    );
  }
}