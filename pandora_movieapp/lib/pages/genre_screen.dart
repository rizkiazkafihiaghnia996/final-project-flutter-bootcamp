import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class GenreScreen extends StatefulWidget {
  final String genreID, genreName;

  const GenreScreen({Key key, this.genreID, this.genreName}) : super(key: key);

  @override
  _GenreScreenState createState() => _GenreScreenState();
}

class _GenreScreenState extends State<GenreScreen> {
  List genreMovies = [];
  int currentPage = 1;

  getGenreMovies() async {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/discover/movie?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${currentPage.toString()}&with_genres=${widget.genreID}&with_watch_monetization_types=flatrate'
    ));
    var jsonData = jsonDecode(response.body);

    genreMovies = jsonData['results'];
    
    print(genreMovies);
    return genreMovies;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getGenreMovies();  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          widget.genreName,
          style: TextStyle(
            fontWeight: FontWeight.bold
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(2),
          child: Container(
            height: 2,
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: FutureBuilder(
                future: getGenreMovies(),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                        ),
                      ),
                    );
                  }
                  else if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        snapshot.error,
                        style: TextStyle(
                          color: Colors.lightBlue,
                        ),
                      ),
                    );
                  }
                  else {
                    return GridView.builder(
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 150,
                        childAspectRatio: 2/3,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 15
                      ),
                      itemCount: genreMovies.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: genreMovies[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + genreMovies[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + genreMovies[index]['poster_path'],
                                    synopsis: genreMovies[index]['overview'],
                                    rating: genreMovies[index]['vote_average'].toString(),
                                    releaseDate: genreMovies[index]['release_date'],
                                    id: genreMovies[index]['id'].toString(),
                                    language: genreMovies[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(
                                    'https://image.tmdb.org/t/p/w500' + genreMovies[index]['poster_path']
                                  )
                                )
                              ),
                            ),
                          ),
                        );
                      }
                    );
                  }
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
              padding: const EdgeInsets.only(top: 5),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.lightBlue,
                    width: 2
                  )
                )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (currentPage == 1) {
                        
                      }
                      else if (currentPage > 1) {
                        setState(() {
                          currentPage--;                
                        });
                        FocusScope.of(context).unfocus();
                        getGenreMovies();
                      }
                    },
                    child: Text(
                      "Previous",
                      style: TextStyle(
                        color: currentPage == 1 ? Colors.grey : Colors.lightBlue,
                        fontSize: 15
                      ),
                    ),
                  ),
                  SizedBox(width: 4),
                  Container(
                    height: 12,
                    width: 1,
                    color: Colors.lightBlue,
                  ),
                  SizedBox(width: 4),
                  InkWell(
                    onTap: () {
                      if (currentPage > 0) {
                        setState(() {
                          currentPage++;                
                        });
                        FocusScope.of(context).unfocus();
                        getGenreMovies();
                      }
                      else if(currentPage == 500) {

                      }
                    },
                    child: Text(
                      "Next",
                      style: TextStyle(
                        color: currentPage == 500 ? Colors.grey : Colors.lightBlue,
                        fontSize: 15
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
