import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'login_screen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String _emailInput;
  String _passwordInput;
  String _retypedPasswordInput;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _retypedPasswordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  bool _passwordVisible = true;
  String errorMessage;

  @override
  void _toggle() {
    setState(() {
      _passwordVisible= !_passwordVisible;  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Image.asset(
                  'assets/img/pandora_logo.png',
                  width: 350,
                )
              ),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: Column(
                  children: [
                    Center(
                      child: TextFormField(
                        style: TextStyle(
                          color: Colors.lightBlue
                        ),
                        controller: _emailController,
                        decoration: InputDecoration(
                          hintText: "E-mail",
                          hintStyle: TextStyle(
                            color: Colors.lightBlue
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _emailInput = value;
                          });
                        },
                      )
                    ),
                    SizedBox(height: 20),
                    Center(
                      child: TextFormField(
                        style: TextStyle(
                          color: Colors.lightBlue
                        ),
                        controller: _passwordController,
                        obscureText: _passwordVisible,
                        decoration: InputDecoration(
                          hintText: "Password",
                          hintStyle: TextStyle(
                            color: Colors.lightBlue
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisible ? Icons.visibility : Icons.visibility_off,
                              color: Colors.lightBlue
                            ),
                            onPressed: () {
                              _toggle();
                            },
                          )
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _passwordInput = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                    Center(
                      child: TextFormField(
                        style: TextStyle(
                          color: Colors.lightBlue
                        ),
                        controller: _retypedPasswordController,
                        obscureText: _passwordVisible,
                        decoration: InputDecoration(
                          hintText: "Confirm Password",
                          hintStyle: TextStyle(
                            color: Colors.lightBlue
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisible ? Icons.visibility : Icons.visibility_off,
                              color: Colors.lightBlue
                            ),
                            onPressed: () {
                              _toggle();
                            },
                          )
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _retypedPasswordInput = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.lightBlue
                      ),
                      child: InkWell(
                        onTap: () async {
                          try{
                            if(_retypedPasswordInput != _passwordInput) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Text(
                                      "Password was not confirmed!"
                                    ),
                                  );
                                }
                              );
                            }
                            else if(_emailInput == null || _passwordInput == null || _passwordInput == null) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Text(
                                      "Please input proper credentials!"
                                    ),
                                  );
                                }
                              );
                            }
                            else {
                              await _firebaseAuth.createUserWithEmailAndPassword(
                                email: _emailController.text, 
                                password: _passwordController.text
                              ).then(
                                (value) {
                                  return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return LoginScreenComponents();
                                      }
                                    )
                                  );
                                }
                              );
                            }
                          }
                          catch(error) {
                            switch (error.code) {
                              case "ERROR_EMAIL_ALREADY_IN_USE":
                              case "account-exists-with-different-credential":
                              case "email-already-in-use":
                                errorMessage = "An account with this email already exists.";
                                break;
                              case "weak-password":
                                errorMessage = "Your Password is too short. Password should be 6 characters at the very least";
                                break;
                              case "ERROR_INVALID_EMAIL":
                              case "invalid-email":
                                errorMessage = "Email address is invalid.";
                                break;
                              default:
                                errorMessage = "Sign up failed. Please try again.";
                                break;
                            }
                            print('Error: $errorMessage');
                            return showDialog(
                              context: context, 
                              builder: (context) {
                                return AlertDialog(
                                  content: Text(
                                    errorMessage
                                  ),
                                );
                              }
                            );
                          }
                        },
                        child: Center(
                          child: Text(
                            "Register",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}