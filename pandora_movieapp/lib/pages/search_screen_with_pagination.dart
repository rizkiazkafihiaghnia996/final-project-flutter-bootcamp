import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_paginator/flutter_paginator.dart';
import 'detail_screen.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:avatar_glow/avatar_glow.dart';

int currentPage = 1;

class SearchResults {
  List<dynamic> searchResults;
  int statusCode;
  String errorMessage;
  int totalResults;
  int nItems;

  SearchResults.fromResponse(http.Response response) {
    this.statusCode = response.statusCode;
    Map jsonData = json.decode(response.body);
    searchResults = jsonData['results'];
    totalResults = jsonData['total_results'];
    nItems = searchResults.length;
  }

  SearchResults.withError(String errorMesaage) {
    this.errorMessage = errorMesaage;
  }
}

Future<SearchResults> getSearch(int currentPage) async {
  print('Page ${currentPage}');
  try {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/search/movie?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&query=' + SearchV2.searchController.text + '&page=' + currentPage.toString() + '&include_adult=false'
    ));
    print(response.body);
    return SearchResults.fromResponse(response);
  }
  catch(e) {
    if (e is IOException) {
      return SearchResults.withError(
        'Please Check Your Internet Connection'
      );
    }
    else {
      print(e.toString());
      return SearchResults.withError(
        'Something Went Wrong'
      );
    }
  }
}

class SearchV2 extends StatefulWidget {
  static TextEditingController searchController = TextEditingController(text: '');
  static GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();

  @override
  _SearchV2State createState() => _SearchV2State();
}

class _SearchV2State extends State<SearchV2> with AutomaticKeepAliveClientMixin<SearchV2>{
  @override
  void dispose() {
    super.dispose();
  }

  List<dynamic> pageItemsGetter(SearchResults searchResults) {
    List searchResultsList = [];
    searchResultsList = searchResults.searchResults;
    return searchResultsList;
  }

  Widget listItemBuilder(value, index) {
    return value['poster_path'] == null ? Container() : value['backdrop_path'] == null ? Container() : value['overview'] == null ? Container() : InkWell(
      onTap: () {
        Navigator.push(
          context, 
          MaterialPageRoute(
            builder: (context) {
              return DetailScreen(
                title: value['title'],
                bannerURL: 'https://image.tmdb.org/t/p/w500' + value['backdrop_path'],
                posterURL: 'https://image.tmdb.org/t/p/w500' + value['poster_path'],
                synopsis: value['overview'],
                rating: value['vote_average'].toString(),
                releaseDate: value['release_date'],
                id: value['id'].toString(),
                language: value['original_language'],
              );
            }
          )
        );
      },
      child: Container(
        height: 200,
        padding: const EdgeInsets.only(bottom: 8, top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 140,
              child: Container(
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                      'https://image.tmdb.org/t/p/w500' + value['poster_path']
                    )
                  )
                ),
              ),
            ),
            SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Flexible(
                    child: Text(
                      value['title'],
                      style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.end,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                    Row(
                      children: [
                        Text(
                          "Release Date: ",
                          style: TextStyle(
                            color: Colors.lightBlue,
                            fontSize: 12
                          ),
                        ),
                        Text(
                          value['release_date'],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                          ),
                        ),
                        SizedBox(width: 3),
                        Container(
                          color: Colors.lightBlue,
                          width: 1,
                          height: 10,
                        ),
                        SizedBox(width: 3),
                        Text(
                          "Rating: ",
                          style: TextStyle(
                            color: Colors.lightBlue,
                            fontSize: 12
                          ),
                        ),
                        value['vote_average'] == 0 ? Text(
                          "-",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                          ),
                        ) : Text(
                          value['vote_average'].toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(height: 15),
                Flexible(
                  child: Text(
                    value['overview'],
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    maxLines: 4,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                )
              ]
            ),
          ),
        ],
      ),
      ),
    );
  }

  Widget loadingWidgetBuilder() {
    return Container(
      alignment: Alignment.center,
      height: 160.0,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
      ),
    );
  }

  Widget errorWidgetBuilder(SearchResults searchResults, retryListener) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Text(
            searchResults.errorMessage,
            style: TextStyle(
              color: Colors.lightBlue,
              fontSize: 16
            ),
          ),
        ),
        InkWell(
          onTap: retryListener,
          child: Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.lightBlue,
              borderRadius: BorderRadius.circular(10)
            ),
            child: Text(
              "Retry",
              style: TextStyle(
                color: Colors.black,
                fontSize: 14
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget emptyListWidgetBuilder(SearchResults searchResults) {
    return Center(
      child: Text(
        "Error 404: No Movies Found",
        style: TextStyle(
          color: Colors.lightBlue,
          fontSize: 14
        ),
      ),
    );
  }

  int totalItemsGetter(SearchResults searchResults) {
    return searchResults.totalResults;
  }

  bool pageErrorChecker(SearchResults searchResults) {
    return searchResults.statusCode != 200;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        child: Container(
          padding: const EdgeInsets.fromLTRB(16.0, 80.0, 16.0, 16.0),
          child: Column(
            children: [
              SearchForm(
                onSearch: (q) {
                  setState(() {
                    SearchV2.searchController.text = q;                 
                  });
                }
              ),
              Expanded(
                flex: 1,
                child: SearchV2.searchController.text.isEmpty ? Container() : Container(
                  child: Paginator.listView(
                    key: SearchV2.paginatorGlobalKey,
                    pageLoadFuture: getSearch, 
                    pageItemsGetter: pageItemsGetter, 
                    listItemBuilder: listItemBuilder, 
                    loadingWidgetBuilder: loadingWidgetBuilder, 
                    errorWidgetBuilder: errorWidgetBuilder, 
                    emptyListWidgetBuilder: emptyListWidgetBuilder, 
                    totalItemsGetter: totalItemsGetter, 
                    pageErrorChecker: pageErrorChecker,
                    scrollPhysics: BouncingScrollPhysics(),
                  )
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class SearchForm extends StatefulWidget {
  final void Function(String search) onSearch;

  const SearchForm({ Key key, this.onSearch }) : super(key: key);

  @override
  _SearchFormState createState() => _SearchFormState();
}

class _SearchFormState extends State<SearchForm> {
  final _formKey = GlobalKey<FormState>();

  var _autoValidate = false;
  var _search;
  bool _isListening = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key:  _formKey,
        //autovalidate: _autoValidate,
        child: TextFormField(
                style: TextStyle(
                  color: Colors.lightBlue
                ),
                controller: SearchV2.searchController,
                decoration: InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(
                    color: Colors.lightBlue
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Colors.lightBlue,
                      width: 2.0
                    )
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Colors.lightBlue,
                      width: 2.0
                    )
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.lightBlue,
                    ), 
                    onPressed: () {
                      final isValid = _formKey.currentState.validate();
                      if(isValid) {
                        widget.onSearch(_search);
                        FocusManager.instance.primaryFocus.unfocus();
                      }
                      else {
                        setState(() {
                          _autoValidate = true;                          
                        });
                      }
                      SearchV2.paginatorGlobalKey.currentState.changeState(
                        pageLoadFuture: getSearch,
                        resetState: true
                      );
                    },
                  ),
                  /*Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                        icon: AvatarGlow(
                          animate: false,
                          glowColor: Colors.white,
                          endRadius: 80,
                          duration: Duration(milliseconds: 2800),
                          repeatPauseDuration: Duration(milliseconds: 100),
                          repeat: true,
                          child: Icon(
                            Icons.mic,
                            color: Colors.lightBlue
                          )
                        ), 
                        onPressed: () {
                          
                        }
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.search,
                          color: Colors.lightBlue,
                        ), 
                        onPressed: () {
                          final isValid = _formKey.currentState.validate();
                          if(isValid) {
                            widget.onSearch(_search);
                            FocusManager.instance.primaryFocus.unfocus();
                          }
                          else {
                            setState(() {
                              _autoValidate = true;                          
                            });
                          }
                        },
                      ),
                    ],
                  ),*/
                ),
                onChanged: (value) {
                  _search = value;
                },
                validator: (value) {
                  if(value.isEmpty) {
                    return 'Please provide a proper search query';
                  }
                  return null;
                },
              ),
      ),
    );
  }
}

class Speech extends StatefulWidget {
  @override
  _SpeechState createState() => _SpeechState();
}

class _SpeechState extends State<Speech> {
  stt.SpeechToText _speech;
  bool isListening = false;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      
    );
  }
}