import 'package:flutter/material.dart';
import 'package:pandora_movieapp/categories/avengers_movies.dart';
import 'package:pandora_movieapp/categories/back_to_the_future_movies.dart';
import 'package:pandora_movieapp/categories/genres.dart';
import 'package:pandora_movieapp/categories/jurassic_park_movies.dart';
import 'package:pandora_movieapp/categories/popular_movies.dart';
import 'package:pandora_movieapp/categories/star_wars_movies.dart';
import 'package:pandora_movieapp/categories/terminator_movies.dart';
import 'package:pandora_movieapp/categories/the_godfather_movies.dart';
import 'package:pandora_movieapp/categories/top_rated_movies.dart';
import 'package:pandora_movieapp/categories/trending_movies.dart';
import 'package:pandora_movieapp/others/category_divider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pandora_movieapp/pages/profile_screen.dart';
import 'package:pandora_movieapp/pages/search_screen.dart';
import 'package:pandora_movieapp/pages/search_screen_with_pagination.dart';
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  User user;
  /* Bottom Nav Bar without Persisted UI State
  int currentTab;
  Widget currentScreen = HomeScreenComponents();*/

  var _selectedPageIndex;
  List<Widget> _pages;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = 0;
    _pages = [
      HomeScreenComponents(),
      SearchV2(),
      ProfileScreen(),
    ];
    _pageController = PageController(initialPage: _selectedPageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  /* Persisting UI State with a Nav Bar Plugin
  PersistentTabController _controller = PersistentTabController(initialIndex: 0);

  List<Widget> _navScreens() {
    return [
      HomeScreenComponents(),
      ProfileScreen()
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
       icon: Icon(Icons.home_outlined),
        title: "Home",
        textStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.lightBlue
        ),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Colors.lightBlue
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.account_circle),
        title: "Profile",
        textStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.lightBlue
        ),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Colors.lightBlue,
      ),
    ];
  }*/

  @override
  Widget build(BuildContext context) {
    if(_firebaseAuth.currentUser != null) {
      print(_firebaseAuth.currentUser.email);
    }
    else {
      print("No user is currently signed in");
    }

    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: _pages,
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.black,
          border: Border(
            top: BorderSide(
              color: Colors.lightBlue,
              width: 2
            )
          )
        ),
        child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              activeIcon: Container(
                padding: const EdgeInsets.all(6),
                margin: const EdgeInsets.only(left: 20, right: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.lightBlue,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.home_rounded,
                      size: 35,
                    ),
                    SizedBox(width: 8),
                    Text(
                      "Home",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15
                      ),
                    )
                  ],
                )
              ),
              label: "",
              icon: Icon(
                Icons.home_outlined,
                size: 35,
              )
            ),
            BottomNavigationBarItem(
              activeIcon: Container(
                padding: const EdgeInsets.all(6),
                margin: const EdgeInsets.only(left: 20, right: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.lightBlue,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.search,
                      size: 35,
                    ),
                    SizedBox(width: 5),
                    Text(
                      "Search",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15
                      ),
                    )
                  ],
                )
              ),
              label: "",
              icon: Icon(
                Icons.search,
                size: 35,
              )
            ),
            BottomNavigationBarItem(
              activeIcon: Container(
                padding: const EdgeInsets.all(6),
                margin: const EdgeInsets.only(left: 20, right: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.lightBlue,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.account_circle_rounded,
                      size: 35,
                    ),
                    SizedBox(width: 8),
                    Text(
                      "Profile",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15
                      ),
                    )
                  ],
                )
              ),
              label: "",
              icon: Icon(
                Icons.account_circle_outlined,
                size: 35,
              )
            ),
          ],
          backgroundColor: Colors.black,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.lightBlue,
          currentIndex: _selectedPageIndex,
          onTap: (selectedPageIndex) {
            setState(() {
              _selectedPageIndex = selectedPageIndex;
              _pageController.jumpToPage(selectedPageIndex);
            });
          },
        ),
      )
      /* Bottom Nav Bar without Persisted UI State
        bottomNavigationBar: BottomAppBar(
          color: Colors.black,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black,
              border: Border(
                top: BorderSide(
                  color: Colors.lightBlue,
                  width: 2
                )
              )
            ),
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentTab = 0;
                      currentScreen = HomeScreenComponents();                       
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.home_outlined,
                        color: currentTab == 0 ? Colors.white : Colors.blue,
                        size: currentTab == 0 ? 40 : 28,
                      ),
                    ],
                  ),
                ),
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentTab = 1;
                      currentScreen = ProfileScreen();                       
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.account_circle_rounded,
                        color: currentTab == 1 ? Colors.grey : Colors.blue,
                        size: currentTab == 1 ? 40 : 28,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),*/
      /* Persisting UI State with a Nav Bar Plugin
      bottomNavigationBar: PersistentTabView(
        context,
        controller: _controller,
        screens: _navScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.black,
        handleAndroidBackButtonPress: true,
        resizeToAvoidBottomInset: true,
        hideNavigationBarWhenKeyboardShows: true,
        bottomScreenMargin: 40,
        decoration: NavBarDecoration(
          border: Border(
            top: BorderSide(
              color: Colors.lightBlue
            )  
          ),
        ),
        popAllScreensOnTapOfSelectedTab: true,
        navBarStyle: NavBarStyle.style9,
      )*/
    );
  }
}

class HomeScreenComponents extends StatefulWidget {
  @override
  _HomeScreenComponentsState createState() => _HomeScreenComponentsState();
}

class _HomeScreenComponentsState extends State<HomeScreenComponents> with AutomaticKeepAliveClientMixin<HomeScreenComponents> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Image.asset(
            'assets/img/pandora_logo.png',
            width: 100,
            height: 100,
          )
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ListView(
                children: [
                  TrendingMovies(),
                  MovieGenres(),
                  SizedBox(height: 15),
                  PopularMovies(),
                  CategoryDivider(),
                  TopRatedMovies(),
                  CategoryDivider(),
                  StarWars(),
                  CategoryDivider(),
                  TheGodfather(),
                  CategoryDivider(),
                  BackToTheFuture(),
                  CategoryDivider(),
                  JurassicPark(),
                  CategoryDivider(),
                  Avengers(),
                  CategoryDivider(),
                  Terminator()
                ]
              ),
            ),
          ],
        )
      )
    );
  }

  @override
  bool get wantKeepAlive => true;
}