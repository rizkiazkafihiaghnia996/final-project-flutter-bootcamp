import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pandora_movieapp/others/category_divider.dart';
import 'package:pandora_movieapp/others/google_sign_in_button.dart';
import 'package:pandora_movieapp/pages/home_screen.dart';
import 'package:pandora_movieapp/pages/register_screen.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return LoginScreenComponents();
  }
}

class LoginScreenComponents extends StatefulWidget {
  @override
  _LoginScreenComponentsState createState() => _LoginScreenComponentsState();
}

class _LoginScreenComponentsState extends State<LoginScreenComponents> {
  String _emailInput;
  String _passwordInput;
  
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  String errorMessage;
  
  bool _passwordVisible = true;
 
  @override
  void _toggle() {
    setState(() {
      _passwordVisible= !_passwordVisible;  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        padding: const EdgeInsets.fromLTRB(16.0, 50.0, 16.0, 16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Image.asset(
                  'assets/img/pandora_logo.png',
                  width: 300,
                )
              ),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: Column(
                  children: [
                    Center(
                      child: TextFormField(
                        style: TextStyle(
                          color: Colors.lightBlue
                        ),
                        controller: _emailController,
                        decoration: InputDecoration(
                          hintText: "E-mail",
                          hintStyle: TextStyle(
                            color: Colors.lightBlue
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _emailInput = value;
                          });
                        },
                      )
                    ),
                    SizedBox(height: 20),
                    Center(
                      child: TextFormField(
                        style: TextStyle(
                          color: Colors.lightBlue
                        ),
                        controller: _passwordController,
                        obscureText: _passwordVisible,
                        decoration: InputDecoration(
                          hintText: "Password",
                          hintStyle: TextStyle(
                            color: Colors.lightBlue
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                              width: 2.0
                            )
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisible ? Icons.visibility : Icons.visibility_off,
                              color: Colors.lightBlue
                            ),
                            onPressed: () {
                              _toggle();
                            },
                          )
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _passwordInput = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.lightBlue
                      ),
                      child: InkWell(
                        onTap: () async {
                          try {
                            if(_emailInput == null || _passwordInput == null) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Text(
                                      "Please input proper credentials!"
                                    ),
                                  );
                                }
                              );
                            }
                            else {
                              await _firebaseAuth.signInWithEmailAndPassword(
                                email: _emailController.text, 
                                password: _passwordController.text
                              ).then(
                                (value) {
                                  return Navigator.pushAndRemoveUntil(
                                    context, 
                                    MaterialPageRoute(
                                      builder: (context) => HomeScreen()
                                    ),
                                    (route) => false
                                  );
                                }
                              );
                            }
                          }
                          catch(error) {
                            switch (error.code) {
                              case "ERROR_EMAIL_ALREADY_IN_USE":
                              case "account-exists-with-different-credential":
                              case "email-already-in-use":
                                errorMessage = "An account with this email already exists.";
                                break;
                              case "ERROR_WRONG_PASSWORD":
                              case "wrong-password":
                                errorMessage = "Wrong email/password combination.";
                                break;
                              case "ERROR_USER_NOT_FOUND":
                              case "user-not-found":
                                errorMessage = "No user found with this email.";
                                break;
                              case "ERROR_USER_DISABLED":
                              case "user-disabled":
                                errorMessage = "User disabled.";
                                break;
                              case "ERROR_TOO_MANY_REQUESTS":
                              case "operation-not-allowed":
                                errorMessage = "Too many requests to log into this account.";
                                break;
                              case "ERROR_OPERATION_NOT_ALLOWED":
                              case "operation-not-allowed":
                                errorMessage = "Server error, please try again later.";
                                break;
                              case "ERROR_INVALID_EMAIL":
                              case "invalid-email":
                                errorMessage = "Email address is invalid.";
                                break;
                              default:
                                errorMessage = "Login failed. Please try again.";
                                break;
                            }
                            print('Error: $errorMessage');
                            return showDialog(
                              context: context, 
                              builder: (context) {
                                return AlertDialog(
                                  content: Text(
                                    errorMessage
                                  ),
                                );
                              }
                            );
                          }
                        },
                        child: Center(
                          child: Text(
                            "Login",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      'Don\'t have an account yet?',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 16
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.lightBlue
                      ),
                      child: InkWell(
                        onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return RegisterScreen();
                                }
                              )
                            );
                        },
                        child: Center(
                          child: Text(
                            "Register",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      "or",
                      style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 16
                      ),
                    ),
                    SizedBox(height: 15),
                    GoogleSignInButton(),
                    SizedBox(height: 15),
                    CategoryDivider(),
                    SizedBox(height: 5),
                    InkWell(
                      onTap: () {

                      },
                      child: Text(
                        "Forgot your Password?",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlue
                        ),
                      )
                    )
                  ],
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}