import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/others/favorite_button.dart';
import 'home_screen.dart';

class DetailScreen extends StatefulWidget {
  final String title, bannerURL, posterURL, synopsis, rating, releaseDate, cast, id, language;

  const DetailScreen({Key key, this.title, this.bannerURL, this.posterURL, this.synopsis, this.rating, this.releaseDate, this.cast, this.id, this.language}) : super(key: key);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  List cast = [];
  List recommendations = [];
  

  getCast() async {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/movie/${widget.id}/credits?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US'
    ));
    var jsonData = jsonDecode(response.body);

    cast = jsonData['cast'];
    return cast;
  }

  getRecommendation() async {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/movie/${widget.id}/recommendations?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&page=1'
    ));
    var jsonData = jsonDecode(response.body);

    recommendations = jsonData['results'];
    //print(recommendations);
    return recommendations;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(
            fontWeight: FontWeight.bold
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.home_outlined
            ),
            color: Colors.lightBlue,
            iconSize: 28,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return HomeScreen();
                  }
                )
              );
            },
          )
        ],
        backgroundColor: Colors.transparent,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(2),
          child: Container(
            color: Colors.lightBlue,
            height: 2.0,
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                        content: Text(
                            'Error: Failed to Initiate Playback. Server under maintenance. We\'re sorry for this inconvenience'
                        )
                    );
                  }
                );    
              },
              child: Stack(
                children: [
                  Container(
                    height: 220,
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(widget.bannerURL),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 50),
                      child: Image.asset(
                        'assets/img/play_button.png',
                        width: 100,
                        height: 100
                      )
                    ),
                  )
                ]
              ), 
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.all(12.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Text(
                              widget.title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.lightBlue,
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                              )
                            ),
                          ),
                          FavoriteButton()
                        ],
                      ),
                      SizedBox(height: 10),
                      Card(
                        color: Colors.lightBlue,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 16, 30, 16),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Column(
                                      children: [
                                        Text(
                                          widget.rating,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25
                                          ),
                                        ),
                                        SizedBox(height: 2.0),
                                        Text(
                                          'out of 10'
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.calendar_today
                                        ),
                                        SizedBox(height: 5.0),
                                        Text(
                                          widget.releaseDate,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold
                                          ),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.language
                                        ),
                                        SizedBox(height: 5.0),
                                        Text(
                                          widget.language.toUpperCase(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        child: Text(
                          widget.synopsis,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        height: 155,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Cast",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.lightBlue,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              height: 120,
                              child: FutureBuilder(
                                future: getCast(),
                                builder: (context, snapshot) {
                                  if(snapshot.data == null) {
                                    return Container(
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                                        ),
                                      ),
                                    );
                                  }
                                  else if (snapshot.hasError) {
                                    return Center(
                                      child: Text(
                                        snapshot.error
                                      ),
                                    );
                                  }
                                  else {
                                    return ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: cast.length < 8 ? cast.length: 8,
                                      itemBuilder: (context, index) {
                                        return  Container(
                                          padding: const EdgeInsets.only(right: 10),
                                          child: Column(
                                            children: [
                                              cast[index]['profile_path'] == null ? Container(
                                                width: 60,
                                                height: 70,
                                                child: Icon(
                                                  Icons.account_circle,
                                                  color: Colors.lightBlue,
                                                  size: 64,                                                ),
                                              ) : Container(
                                                width: 60,
                                                height: 70,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                    image: NetworkImage(
                                                      'https://image.tmdb.org/t/p/w500' + cast[index]['profile_path']
                                                    ),
                                                  fit: BoxFit.fill
                                                )
                                              ),
                                            ),
                                            SizedBox(height: 5),
                                            Container(
                                              width: 70,
                                              child: Center(
                                                child: Text(
                                                  cast[index]['name'],
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.lightBlue
                                                  ),
                                                ),
                                              )
                                            )
                                          ]
                                        ),
                                       );
                                      }
                                    );
                                  }
                                },
                              )
                            )
                          ]
                        )
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "You might also like",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.lightBlue,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              height: 210,
                              child: FutureBuilder(
                                future: getRecommendation(),
                                builder: (context, snapshot) {
                                  if (snapshot.data == null) {
                                    return Container(
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                                        ),
                                      ),
                                    );
                                  }
                                  else if (snapshot.hasError) {
                                    return Center(
                                      child: Text(
                                        snapshot.error
                                      ),
                                    );
                                  }
                                  else {
                                    return ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: recommendations.length,
                                      itemBuilder: (context, index) {
                                        return recommendations[index]['poster_path'] == null ? Container() : InkWell(
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) {
                                                  return DetailScreen(
                                                    title: recommendations[index]['title'],
                                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + recommendations[index]['backdrop_path'],
                                                    posterURL: 'https://image.tmdb.org/t/p/w500' + recommendations[index]['poster_path'],
                                                    synopsis: recommendations[index]['overview'],
                                                    rating: recommendations[index]['vote_average'].toStringAsFixed(1),
                                                    releaseDate: recommendations[index]['release_date'],
                                                    id: recommendations[index]['id'].toString(),
                                                    language: recommendations[index]['original_language']
                                                  );
                                                }
                                              )
                                            );
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.only(right: 5),
                                            width: 140,
                                            child: Container(
                                              height: 200,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: NetworkImage(
                                                    'https://image.tmdb.org/t/p/w500' + recommendations[index]['poster_path']
                                                  )
                                                )
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}