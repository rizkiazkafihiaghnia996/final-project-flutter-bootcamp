import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class StarWars extends StatefulWidget {

  @override
  _StarWarsState createState() => _StarWarsState();
}

class _StarWarsState extends State<StarWars> {
  List starWars = [];
  
  getStarWars() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/collection/10?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    starWars = jsonData['parts'];

    //print(starWars);
    return starWars;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getStarWars();  
    });
  }

 @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "A Long Time Ago In a Galaxy Far, Far Away...",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 17
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getStarWars(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: starWars.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: starWars[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + starWars[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + starWars[index]['poster_path'],
                                    synopsis: starWars[index]['overview'],
                                    rating: starWars[index]['vote_average'].toString(),
                                    releaseDate: starWars[index]['release_date'],
                                    id: starWars[index]['id'].toString(),
                                    language: starWars[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + starWars[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}