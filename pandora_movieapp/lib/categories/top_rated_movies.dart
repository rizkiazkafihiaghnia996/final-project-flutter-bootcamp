import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class TopRatedMovies extends StatefulWidget {
  @override
  _TopRatedMoviesState createState() => _TopRatedMoviesState();
}

class _TopRatedMoviesState extends State<TopRatedMovies> {
  List topRatedMovies = [];

  getTopRated() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/movie/top_rated?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&page=1&region=id"
    ));
    var jsonData = jsonDecode(response.body);

    topRatedMovies = jsonData['results'];

    //print(topRatedMovies);
    return topRatedMovies;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getTopRated();  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Top Rated",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getTopRated(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error,
                      style: TextStyle(
                        color: Colors.lightBlue,
                      ),
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: topRatedMovies.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: topRatedMovies[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + topRatedMovies[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + topRatedMovies[index]['poster_path'],
                                    synopsis: topRatedMovies[index]['overview'],
                                    rating: topRatedMovies[index]['vote_average'].toString(),
                                    releaseDate: topRatedMovies[index]['release_date'],
                                    id: topRatedMovies[index]['id'].toString(),
                                    language: topRatedMovies[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + topRatedMovies[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}