import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/genre_screen.dart';
import 'package:pandora_movieapp/pages/genre_screen_with_pagination.dart';

class MovieGenres extends StatefulWidget {
  @override
  _MovieGenresState createState() => _MovieGenresState();
}

class _MovieGenresState extends State<MovieGenres> {
  List genres = [];

  getGenres() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/genre/movie/list?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    genres = jsonData['genres'];
    return genres;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getGenres();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      child: FutureBuilder(
          future: getGenres(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                  ),
                ),
              );
            }
            else if (snapshot.hasError){
              return Center(
                child: Text(
                  snapshot.error
                ),
              );
            }
            else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: genres.length,
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(
                            builder: (context) {
                              return GenreScreenV2(
                                genreID: genres[index]['id'].toString(),
                                genreName: genres[index]['name'],
                              );
                            }
                          )
                        );
                      },
                      child: Container(
                        height: 45,
                        decoration: BoxDecoration(
                          color: Colors.lightBlue,
                          borderRadius: BorderRadius.circular(6)
                        ),
                        margin: const EdgeInsets.only(left: 5, right: 5),
                        padding: const EdgeInsets.only(left: 16, right: 16),
                          child: Center(
                            child: Text(
                              genres[index]["name"],
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      );
                  },
                );
              }
            },
        ),
    );
  }
}