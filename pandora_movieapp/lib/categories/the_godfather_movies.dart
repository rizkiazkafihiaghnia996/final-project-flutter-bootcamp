import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class TheGodfather extends StatefulWidget {

  @override
  _TheGodfatherState createState() => _TheGodfatherState();
}

class _TheGodfatherState extends State<TheGodfather> {
  List theGodfather = [];
  
  getTheGodfather() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/collection/230?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    theGodfather = jsonData['parts'];

    //print(theGodfather);
    return theGodfather;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getTheGodfather();  
    });
  }

 @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "An Offer You Can't Refuse",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getTheGodfather(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: theGodfather.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: theGodfather[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + theGodfather[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + theGodfather[index]['poster_path'],
                                    synopsis: theGodfather[index]['overview'],
                                    rating: theGodfather[index]['vote_average'].toString(),
                                    releaseDate: theGodfather[index]['release_date'],
                                    id: theGodfather[index]['id'].toString(),
                                    language: theGodfather[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + theGodfather[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}