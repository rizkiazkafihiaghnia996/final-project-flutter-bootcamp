import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class BackToTheFuture extends StatefulWidget {

  @override
  _BackToTheFutureState createState() => _BackToTheFutureState();
}

class _BackToTheFutureState extends State<BackToTheFuture> {
  List backToTheFuture = [];

  getBTTF() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/collection/264?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    backToTheFuture = jsonData['parts'];

    return backToTheFuture;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    getBTTF();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "You'll Be Going at 88 MPH on This One",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getBTTF(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: backToTheFuture.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: backToTheFuture[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + backToTheFuture[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + backToTheFuture[index]['poster_path'],
                                    synopsis: backToTheFuture[index]['overview'],
                                    rating: backToTheFuture[index]['vote_average'].toString(),
                                    releaseDate: backToTheFuture[index]['release_date'],
                                    id: backToTheFuture[index]['id'].toString(),
                                    language: backToTheFuture[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + backToTheFuture[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}