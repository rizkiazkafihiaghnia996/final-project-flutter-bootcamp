import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class JurassicPark extends StatefulWidget {

  @override
  _JurassicParkState createState() => _JurassicParkState();
}

class _JurassicParkState extends State<JurassicPark> {
  List jurassicPark = [];

  getJurassicPark() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/collection/328?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    jurassicPark = jsonData['parts'];

    return jurassicPark;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    getJurassicPark();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Life Finds a Way",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getJurassicPark(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: jurassicPark.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: jurassicPark[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + jurassicPark[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + jurassicPark[index]['poster_path'],
                                    synopsis: jurassicPark[index]['overview'],
                                    rating: jurassicPark[index]['vote_average'].toString(),
                                    releaseDate: jurassicPark[index]['release_date'],
                                    id: jurassicPark[index]['id'].toString(),
                                    language: jurassicPark[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + jurassicPark[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}