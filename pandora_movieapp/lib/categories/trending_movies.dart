import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class TrendingMovies extends StatefulWidget {

  @override
  _TrendingMoviesState createState() => _TrendingMoviesState();
}

class _TrendingMoviesState extends State<TrendingMovies> {
  List trendingMovies = [];
  
  getTrending() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/trending/movie/week?api_key=c94bd944527e52e2c1a033f4fa56054f"
    ));
    var jsonData = jsonDecode(response.body);

    trendingMovies = jsonData['results'];

    //print(trendingMovies);
    return trendingMovies;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getTrending();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 210,
            child: FutureBuilder(
              future: getTrending(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return CarouselSlider.builder(
                    itemCount: trendingMovies.length,
                    options: CarouselOptions(
                      height: 210,
                      enableInfiniteScroll: true,
                      enlargeCenterPage: true,
                      autoPlay: true,
                      autoPlayInterval: const Duration(seconds: 10)
                    ),
                    itemBuilder: (context, index, realIndex) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return DetailScreen(
                                  title: trendingMovies[index]['title'],
                                  bannerURL: 'https://image.tmdb.org/t/p/w500' + trendingMovies[index]['backdrop_path'],
                                  posterURL: 'https://image.tmdb.org/t/p/w500' + trendingMovies[index]['poster_path'],
                                  synopsis: trendingMovies[index]['overview'],
                                  rating: trendingMovies[index]['vote_average'].toString(),
                                  releaseDate: trendingMovies[index]['release_date'],
                                  id: trendingMovies[index]['id'].toString(),
                                  language: trendingMovies[index]['original_language'],
                                );
                              }
                            )
                          );
                        },
                        child: Container(
                          width: 500,
                          child: Stack(
                            children: [
                              Container(
                                height: 200,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: NetworkImage(
                                      'https://image.tmdb.org/t/p/w500' + trendingMovies[index]['backdrop_path']
                                    )
                                  )
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(bottom: 30, left: 5),
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    trendingMovies[index]['title'],
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.lightBlue,
                                      fontSize: 18,
                                      shadows: [
                                        Shadow(
                                          offset: Offset(-1.5, -1.5),
                                          color: Colors.black
                                        ),
                                        Shadow(
                                          offset: Offset(1.5, -1.5),
                                          color: Colors.black
                                        ),
                                        Shadow(
                                          offset: Offset(1.5, 1.5),
                                          color: Colors.black
                                        ),
                                        Shadow(
                                          offset: Offset(-1.5, 1.5),
                                          color: Colors.black
                                        ),
                                      ]
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              )
                            ]
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            )
          )
        ],
      ),
    );
  }
}