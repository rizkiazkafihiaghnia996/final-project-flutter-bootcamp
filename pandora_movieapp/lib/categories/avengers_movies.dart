import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pandora_movieapp/pages/detail_screen.dart';

class Avengers extends StatefulWidget {

  @override
  _AvengersState createState() => _AvengersState();
}

class _AvengersState extends State<Avengers> {
  List avengers = [];

  getAvengers() async {
    var response = await http.get(Uri.parse(
      "https://api.themoviedb.org/3/collection/86311?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US"
    ));
    var jsonData = jsonDecode(response.body);

    avengers = jsonData['parts'];

    return avengers;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    getAvengers();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "AVENGERS!... Assemble",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getAvengers(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: avengers.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: avengers[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + avengers[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + avengers[index]['poster_path'],
                                    synopsis: avengers[index]['overview'],
                                    rating: avengers[index]['vote_average'].toString(),
                                    releaseDate: avengers[index]['release_date'],
                                    id: avengers[index]['id'].toString(),
                                    language: avengers[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + avengers[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}