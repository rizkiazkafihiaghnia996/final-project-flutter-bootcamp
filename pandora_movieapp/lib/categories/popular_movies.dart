import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:pandora_movieapp/pages/detail_screen.dart';

class PopularMovies extends StatefulWidget {
  @override
  _PopularMoviesState createState() => _PopularMoviesState();
}

class _PopularMoviesState extends State<PopularMovies> {
  List popular = [];

  getPopular() async {
    var response = await http.get(Uri.parse(
      'https://api.themoviedb.org/3/movie/popular?api_key=c94bd944527e52e2c1a033f4fa56054f&language=en-US&page=1&region=us'
    ));
    var jsonData = jsonDecode(response.body);

    popular = jsonData['results'];
    //print(popular);
    return popular;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getPopular();    
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Popular Right Now",
            style: TextStyle(
              color: Colors.lightBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 210,
            child: FutureBuilder(
              future: getPopular(),
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      ),
                    ),
                  );
                }
                else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      snapshot.error
                    ),
                  );
                }
                else {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: popular.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return DetailScreen(
                                    title: popular[index]['title'],
                                    bannerURL: 'https://image.tmdb.org/t/p/w500' + popular[index]['backdrop_path'],
                                    posterURL: 'https://image.tmdb.org/t/p/w500' + popular[index]['poster_path'],
                                    synopsis: popular[index]['overview'],
                                    rating: popular[index]['vote_average'].toString(),
                                    releaseDate: popular[index]['release_date'],
                                    id: popular[index]['id'].toString(),
                                    language: popular[index]['original_language']
                                  );
                                }
                              )
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.only(right: 8),
                            width: 140,
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  'https://image.tmdb.org/t/p/w500' + popular[index]['poster_path']
                                )
                              )
                            ),
                          ),
                       ),
                     );
                    },
                  );
                }
              },
            )
          )
        ],
      )
    );
  }
}