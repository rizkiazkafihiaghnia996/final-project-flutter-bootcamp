import 'package:flutter/material.dart';
import 'package:pandora_movieapp/authentication/google_sign_in.dart';
import 'package:pandora_movieapp/pages/home_screen.dart';

class GoogleSignInButton extends StatelessWidget {
  Future<void> signInWithGoogle() async {
    await GoogleAuth().signInWithGoogle();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        signInWithGoogle().then(
          (value) => Navigator.pushAndRemoveUntil(
            context, 
            MaterialPageRoute(
              builder: (context) => HomeScreen()
            ), 
            (route) => false
          )
        );
      },
      child: Ink(
        color: Color(0xFF4285F4),
        child: Padding(
          padding: EdgeInsets.all(6),
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Image.asset(
                'assets/img/png-transparent-google-company-text-logo.png',
                width: 40,
                height: 40,
              ),
              SizedBox(width: 12),
              Text(
                'Sign in with Google',
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }
}