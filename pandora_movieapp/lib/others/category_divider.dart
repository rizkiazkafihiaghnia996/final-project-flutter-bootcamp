import 'package:flutter/material.dart';

class CategoryDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 9,
          child: Container(
            color: Colors.lightBlue,
            margin: const EdgeInsets.only(top: 7.0),
          ),
        ),
        SizedBox(
          height: 7,
        )
      ]
    );
  }
}